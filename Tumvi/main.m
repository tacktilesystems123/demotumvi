//
//  main.m
//  Tumvi
//
//  Created by Santiago Beltran on 6/8/15.
//  Copyright (c) 2015 Tumvi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
